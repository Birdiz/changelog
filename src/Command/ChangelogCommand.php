<?php

declare(strict_types=1);

namespace Birdiz\ChangelogBundle\Command;

use Birdiz\ChangelogBundle\Contract\FileHandlerInterface;
use Birdiz\ChangelogBundle\Service\Handler\ChangelogHandler;
use DateTime;
use LogicException;
use RuntimeException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\HelperInterface;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

/** @codeCoverageIgnore */
#[AsCommand(name: 'app:changelog', description: 'Populate easily your changelog.md.')]
class ChangelogCommand extends Command
{
    private ?string $version;
    private ChangelogHandler $changelogHandler;

    public function __construct(string $version)
    {
        $this->version = $version;
        $this->changelogHandler = new ChangelogHandler();

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp(
                <<<'EOF'
This command allows your to insert a new entry in your changelog.md. Please note: if the file changelog.md does't 
exist, it will be created automaticaly.
Also, if the given version doesn't exist in the changelog.md, it will be placed on top.
EOF
            );

        $this
            ->addArgument(
                ChangelogHandler::VERSION,
                InputArgument::OPTIONAL,
                'Version to append the entry',
                $this->version
            )
            ->addOption(
                ChangelogHandler::TITLE,
                't',
                InputOption::VALUE_REQUIRED,
                'Title of the entry',
                'New Entry'
            )
            ->addOption(
                ChangelogHandler::DESCRIPTION,
                'd',
                InputOption::VALUE_REQUIRED,
                'Content of the entry',
                ''
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $version = $input->getArgument(ChangelogHandler::VERSION);
        $title = self::askTitle($input, $output, $helper);
        $description = self::askDescription($input, $output, $helper);

        $this->checkChangelog();
        $this->changelogHandler->injectString(
            ChangelogHandler::CHANGELOG_PATH,
            [
                ChangelogHandler::VERSION => $version,
                ChangelogHandler::TITLE => $title,
                ChangelogHandler::DESCRIPTION => $description,
            ],
            $version
        );

        $style->success('Your changelog is updated !');

        return Command::SUCCESS;
    }

    private function checkChangelog(): void
    {
        $changelogFile = $this->changelogHandler->findOrCreateFile(ChangelogHandler::CHANGELOG_PATH);
        if (FileHandlerInterface::FILE_CREATED === $changelogFile) {
            $this->changelogHandler->getFileSystem()->appendToFile(
                ChangelogHandler::CHANGELOG_PATH,
                "Changelog\n".ChangelogHandler::TITLE_SEPARATOR
            );
        }
    }

    private static function askTitle(InputInterface $input, OutputInterface $output, HelperInterface $helper): string
    {
        if (!$helper instanceof QuestionHelper) {
            throw new LogicException();
        }

        $question = new Question("Please enter the title of the entry:\n", (new DateTime())->format('d-m-y'));

        return $helper->ask($input, $output, $question);
    }

    private static function askDescription(
        InputInterface $input,
        OutputInterface $output,
        HelperInterface $helper
    ): string {
        if (!$helper instanceof QuestionHelper) {
            throw new LogicException();
        }

        $question = new Question("Please enter the content:\n", null);
        $question->setValidator(
            static function (?string $answer) {
                if (null === $answer || '' === $answer) {
                    throw new RuntimeException('The description is required.');
                }

                return $answer;
            }
        );

        return $helper->ask($input, $output, $question);
    }
}
