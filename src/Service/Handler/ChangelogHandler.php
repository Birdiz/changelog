<?php

declare(strict_types=1);

namespace Birdiz\ChangelogBundle\Service\Handler;

use Birdiz\ChangelogBundle\Contract\FileHandlerInterface;
use LogicException;
use Symfony\Component\Filesystem\Filesystem;

class ChangelogHandler implements FileHandlerInterface
{
    public const TITLE_SEPARATOR = '========================';
    public const VERSION_SEPARATOR = '------------';
    public const CHANGELOG_PATH = './CHANGELOG.md';

    public const TITLE = 'title';
    public const DESCRIPTION = 'description';
    public const VERSION = 'version';

    public function __construct(private readonly Filesystem $filesystem = new Filesystem())
    {
    }

    public function getFileSystem(): Filesystem
    {
        return $this->filesystem;
    }

    public function findOrCreateFile(?string $filepath): int
    {
        if (false === is_string($filepath) || '' === $filepath) {
            throw new LogicException('Filepath is missing.');
        }

        if (false === $this->filesystem->exists($filepath)) {
            $this->filesystem->touch($filepath);

            return static::FILE_CREATED;
        }

        return static::FILE_FOUND;
    }

    /**
     * @param array<mixed> $data
     *
     * @codeCoverageIgnore
     */
    public function injectString(string $path, array $data, ?string $search = null): void
    {
        $lines = file($path, FILE_IGNORE_NEW_LINES);

        if (false !== $lines) {
            $versionIdx = array_search($search, $lines, true);
            if (is_string($search) && false !== $versionIdx) {
                self::insertNewEntry($lines, $data, $versionIdx);
            }
            if (false === $versionIdx) {
                self::insertNewVersion($lines, $data);
            }

            file_put_contents($path, implode("\n", $lines));
        }
    }

    /**
     * @param array<int, string> $lines
     * @param array<mixed>       $data
     *
     * @codeCoverageIgnore
     */
    private static function insertNewEntry(array &$lines, array $data, int $versionIdx): void
    {
        array_splice(
            $lines,
            $versionIdx + 2, // version + version separator
            0,
            ['* '.$data[static::TITLE].' : '.$data[static::DESCRIPTION]]
        );
    }

    /**
     * @param array<int, string> $lines
     * @param array<mixed>       $data
     *
     * @codeCoverageIgnore
     */
    private static function insertNewVersion(array &$lines, array $data): void
    {
        // add version subtitle just after the title separator
        $titleSeparatorIdx = array_search(static::TITLE_SEPARATOR, $lines, true);
        array_splice(
            $lines,
            $titleSeparatorIdx + 1,
            0,
            [$data[static::VERSION], static::VERSION_SEPARATOR]
        );

        // add the new entry
        $versionSeparatorIdx = array_search(static::VERSION_SEPARATOR, $lines, true);
        array_splice(
            $lines,
            $versionSeparatorIdx + 1,
            0,
            ['* '.$data[static::TITLE].' : '.$data[static::DESCRIPTION]."\n"]
        );
    }
}
