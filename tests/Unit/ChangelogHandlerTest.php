<?php

namespace Birdiz\ChangelogBundle\Tests\Unit;

use Birdiz\ChangelogBundle\Contract\FileHandlerInterface;
use Birdiz\ChangelogBundle\Service\Handler\ChangelogHandler;
use LogicException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;

class ChangelogHandlerTest extends TestCase
{
    public function testGetFileSystem(): void
    {
        $changelogHandler = new ChangelogHandler();

        static::assertSame(Filesystem::class, get_class($changelogHandler->getFileSystem()));
    }

    public function testCreateFile(): void
    {
        $changelogHandler = new ChangelogHandler();

        static::assertSame(FileHandlerInterface::FILE_CREATED, $changelogHandler->findOrCreateFile('test'));

        $changelogHandler->getFileSystem()->remove('test');
    }

    public function testFindFile(): void
    {
        $changelogHandler = new ChangelogHandler();

        static::assertContains(
            $changelogHandler->findOrCreateFile(ChangelogHandler::CHANGELOG_PATH), [
                FileHandlerInterface::FILE_CREATED,
                FileHandlerInterface::FILE_FOUND,
            ]
        );
    }

    public function testFindOrCreateFilePathEmpty(): void
    {
        $this->expectException(LogicException::class);
        $changelogHandler = new ChangelogHandler();

        static::assertSame(
            FileHandlerInterface::FILE_FOUND,
            $changelogHandler->findOrCreateFile('')
        );
    }
}
